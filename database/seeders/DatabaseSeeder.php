<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {


        \App\Models\User::factory()->create([
            'name' => 'Prashant Chauhan',
            'email' => 'customer@example.com'
        ]);

        \App\Models\User::factory(39)->create();

        \App\Models\Administrator::factory()->create([
            'firstname' => 'Nurul',
            'lastname' => 'Hasan',
            'email' => 'admin@admin.com'
        ]);

        \App\Models\Administrator::factory(5)->create();


    }
}
